#设置编码字符集为UTF8
SET NAMES UTF8;
#丢弃数据库test,如果存在的话
DROP DATABASE IF EXISTS test;
#创建数据库test
CREATE DATABASE test CREATESET=UTF8;
#进入数据库 test
USE test;

#创建用户信息表test_user
CREATE TABLE test_user(
  uid INT PRIMARY KEY NOT NULL AUTO_INCREMENT,
  nickName VARCHAR(32),
  avatarUrl VARCHAR(1024),
  gender INT,
  country VARCHAR(16),
  language VARCHAR(16),
  province VARCHAR(16),
  city VARCHAR(16)
);